# try to simulate some liquid dynamics

import simple_vector

# get a fluid container
const cont_width*  = 30
const cont_height* = 20
const cont_size*   = cont_width * cont_height

var water*: array[cont_size, Vector2D]

# fill with water
for cell in 0..<cont_size:
    water[cell] = Vector(0, 0)

proc apply_force*(cell, force: Vector2D): void =
    let force_x = force.x
    let force_y = force.y
    discard cell.add(x: force_x, y: force_y)
    discard