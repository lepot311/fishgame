import logging
import math
import random
import sequtils
import tables

import simple_vector

import ./entity
import ./physics


# init RNG
randomize()


type
  Tank* = ref object
    w*       : int
    h*       : int
    entities*: seq[Entity]
    food*    : seq[Food]
    food_max*: int
    poop*    : seq[Poop]


proc new_tank*(w, h: int): Tank =
  new result
  result.w        = w
  result.h        = h
  result.food_max = 4

proc get_random_point(tank: Tank): Point =
  result.x = (float) rand(1..tank.w-1)
  result.y = (float) rand(1..tank.h-1)

proc get_starting_pos*(tank: Tank, entity: Entity): Point =
  result = tank.get_random_point()
  # TODO crawlers should start on ground

proc get_distance_to_food(entity: Entity, food: Food): float =
  return (entity.pos.x - food.pos.x) + (entity.pos.y - food.pos.y)

proc get_nearest_food(tank: Tank, entity: Entity): Food =
  var distances = initTable[Food, float]()

  for food in tank.food:
    var distance = entity.get_distance_to_food(food)
    #distances[Food] = distance
    # TODO sort and take smallest distance

proc update_pos*(tank: Tank, entity: Entity): void =
  # update position of entity
  entity.pos.x += entity.vec_local.x

  if entity.pos.x < 0:
    entity.pos.x = 0
    entity.vec_local.x = 0
    #entity.vec_local.x = +entity.vec_local.x / 2

  if entity.pos.x >= (float) tank.w:
    entity.pos.x = (float) tank.w-1
    entity.vec_local.x = 0
    #entity.vec_local.x = -entity.vec_local.x / 2

  entity.pos.y += entity.vec_local.y

  if entity.pos.y < 0:
    entity.pos.y = 0
    entity.vec_local.y = 0
    #entity.vec_local.y = +entity.vec_local.y / 2

  if entity.pos.y >= (float) tank.h:
    entity.pos.y = (float) tank.h-1
    entity.vec_local.y = 0
    #entity.vec_local.y = -entity.vec_local.y / 2

  discard

proc null_goal(entity: Entity): void =
  entity.goal = Positionable()
  entity.goal.pos = entity.pos

proc add_poop(tank: Tank, pos: Point): void =
  let poop = new_poop()
  poop.pos = Point(x: pos.x, y: pos.y)
  tank.poop.add(poop)

proc update(tank: Tank, entity: Entity): void =
  entity.state_tick += 1
  entity.food_tick += 1

  if entity.is_hungry():
    # is there food?
    if tank.food.len > 0:
        entity.set_goal tank.food[0]

  if entity.has_goal:
    if entity.at_goal:
      if entity.goal of Food:
        if entity.goal.eaten:
          entity.sleep()
        else:
          entity.goal.eaten = true
          entity.food += 1
          entity.food_eaten += 1
          entity.food_tick = 0
          entity.hunger = 0
      entity.sleep()
    else:
      entity.move_to(entity.goal)

  if entity.state == EntityState.IDLE:
    var random_goal = Positionable()
    random_goal.pos = tank.get_random_point()
    entity.set_goal random_goal

  # poop
  if entity.food_tick == 100 and entity.food > 0:
    entity.food -= 1
    entity.food_tick = 0
    tank.add_poop(entity.pos)

  # update hunger
  if entity.appetite > entity.food:
    entity.hunger = min(1, entity.hunger + 0.05)
  else:
    entity.hunger = max(0, entity.hunger - 0.1)

  # update entity state
  if entity.state == EntityState.SLEEPING:
    # hit the brakes
    entity.vec_local.x *= max(entity.speed_acc, 0)
    entity.vec_local.y *= max(entity.speed_acc, 0)

    if ((float) entity.state_tick) >= entity.sleep_secs:
      entity.state = EntityState.IDLE
      return

  # species-aware movement
  # stay close to same
  # for other in tank.entities:
  #   if other.species == entity.species:
  #     entity.school_with other

  tank.update_pos(entity)

  # collide with entity
  # for f in tank.entities:
  #   if f == entity:
  #     continue
  #   else:
  #     if collides_with(entity, f):
  #       echo &"collision! {f.name} and {entity.name}"
  #       entity.vec_total.x += f.mass * -1
  #       entity.vec_total.y += f.mass * -1

  discard

proc add_entity*(tank: Tank, entity: Entity): void =
  entity.pos = tank.get_starting_pos(entity)
  tank.entities.add entity
  discard

proc add_food*(tank: Tank): void =
  let food   = new_food()
  food.pos   = tank.get_random_point()
  food.pos.y = 0  # drop in top of tank

  tank.food.add food
  discard

proc update*(tank: Tank, thing: Positionable): void =
  thing.vec_total = GRAVITY

  thing.pos.y += thing.vec_total.y
  if thing.pos.y >= (float) tank.h-1:
    thing.pos.y = (float) tank.h-1
  discard

proc update*(tank: Tank): void =
  for food in tank.food:
    tank.update(food)

  for poop in tank.poop:
    tank.update(poop)

  for entity in tank.entities:
    tank.update(entity)

  discard