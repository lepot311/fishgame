import colors
import math
import strformat
import strutils
import system
import terminal

import ./entity
import ./tank
import ./physics


const CHAR_WATER = " "


proc nearest_int(n: float): int =
  (int)round(n)

proc get_speedometer*(entity: Entity): string =
  const speedo_length: int = 10
  entity.vel = abs(entity.vec_local.x + entity.vec_local.y)
  let n_bars = (int) abs( (entity.vel / entity.speed_max) * (float) speedo_length )
  result = ""

  for i in 0..<speedo_length:
    result.add(ansiForegroundColorCode(colGreen))
    #if entity.vec.x > 0:
      #result.add(ansiForegroundColorCode(colGreen))
    #else:
      #result.add(ansiForegroundColorCode(colRed))

    if i >= n_bars:
      result.add(ansiForegroundColorCode(colWhite))

    result.add("|")

  # back to default color
  result.add(ansiForegroundColorCode(colWhite))


# proc to_string*(entity: Entity): string =
    # result = &"{ansiForegroundColorCode(entity.species.color)}{entity.name[0]}{ansiForegroundColorCode(colWhite)}"

proc to_string*(point: Point): string =
  return fmt"({point.x}, {point.y})"

proc to_string*(food: Food): string =
  return "o"

#[
proc draw_tank_borders(tank: Tank): string =
  var result: seq[string]

  for row in 0..tank.h:
    for col in 0..tank.w:
      # draw corners
      # top left
      if row == 0 and col == 0:
        result.add("+")

      # top right
      elif row == 0 and col == tank.w:
        result.add("+")

      # bottom left
      elif row == tank.h and col == 0:
        result.add("+")

      # bottom right
      elif row == tank.h and col == tank.w:
        result.add("+")

      # draw top/bottom
      elif row == 0 or row == tank.h:
        result.add("-")

      # draw sides
      elif col == 0 or col == tank.w:
        result.add("|")

  # overlay the fish
  for fish in tank.fish:
    var offset: int = (nearest_int_pos_y(fish.pos.y) * tank.w) + nearest_int_pos_x(fish.pos.x)
    result[offset] = &"{ansiForegroundColorCode(fish.color)}{fish.name[0]}{ansiForegroundColorCode(colWhite)}"
]#

# proc to_string*(tank: Tank): string =
#   result = ""

#   var pixels: seq[seq[string]]

#   # initialize pixels to `.`
#   for rows in 0..<tank.h:
#     var row: seq[string]

#     for col in 0..<tank.w:
#       row.add(CHAR_WATER)

#     row.add("\n")
#     pixels.add(row)

#   # draw food
#   for food in tank.food:
#     var pos_x = nearest_int(food.pos.x)
#     var pos_y = nearest_int(food.pos.y)
#     pixels[pos_y][pos_x] = food.to_string

#   # draw fish
#   for entity in tank.entities:
#     var pos_x = nearest_int(entity.pos.x)
#     var pos_y = nearest_int(entity.pos.y)

#     var goal_x = nearest_int(entity.goal.pos.x)
#     var goal_y = nearest_int(entity.goal.pos.y)

#     # TODO: re-enable
#     # write goal
#     #if entity.has_goal:
#     #  if entity.goal of Food:
#     #    pixels[goal_y][goal_x] = &"o"
#     #  else:
#     #    pixels[goal_y][goal_x] = &"{ansiForegroundColorCode(entity.species.color)},{ansiForegroundColorCode(colWhite)}"

#     pixels[pos_y][pos_x] = entity.to_string


#   # convert to a string
#   for row in pixels:
#     for col in row:
#       result.add(col)

#   return result

proc max_entity_name_length*(tank: Tank): int =
  result = 0
  for entity in tank.entities:
    result = max(result, entity.name.len)
