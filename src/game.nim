import logging
import math
import os
import sequtils
import strformat
import system

import illwill

import ./entity
import ./tank
import ./renderer


# logging
var log = newFileLogger("log")
addHandler(log)

# set up some globals
var tick: int

const tps  = 4
const seconds_food = 4
const wait = (int) 1000 / tps

proc exitProc() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit(0)

proc nearest_int(n: float): int =
  (int)round(n)

proc draw_entity_info*(tank: Tank, tb: var TerminalBuffer): void =
  tb.setCursorPos((Natural) 0, (Natural) tank.h+2)

  var fields: seq[string] = @[
    "Name",
    "PosX",
    "PosY",
    "GoalX",
    "GoalY",
    "VecX",
    "VecY",
    "Velocity",
    "Eaten",
    "Hunger",
    "State",
  ]

  # write header
  for field in fields:
    tb.write fgWhite, &"{field:>10}"

  # write each entity's info
  tb.setCursorPos(0, tb.getCursorYPos+1)

  for entity in tank.entities:
    tb.setCursorPos(0, tb.getCursorYPos+1)
    tb.write entity.species.color, &"{entity.name:>10}"
    tb.write fgWhite, &"{entity.pos.x:>10.2f}"
    tb.write fgWhite, &"{entity.pos.y:>10.2f}"
    tb.write fgWhite, &"{entity.goal.pos.x:>10.2f}"
    tb.write fgWhite, &"{entity.goal.pos.y:>10.2f}"
    tb.write fgWhite, &"{entity.vec_local.x:>10.2f}"
    tb.write fgWhite, &"{entity.vec_local.y:>10.2f}"
    tb.write fgWhite, &"{entity.vel:>10.2f}"
    # tb.write fgWhite, &"{entity.get_speedometer()} "
    tb.write fgWhite, &"{entity.food:>8}/{entity.appetite}"
    tb.write fgWhite, &"{entity.hunger:>10}"
    tb.write fgWhite, &"{entity.state:>10}"
  #   #if entity.state == EntityState.SLEEPING:
  #   #  stdout.write &"{entity.secs_left_to_sleep:>2.2f}"

proc draw*(tank: Tank, tb: var TerminalBuffer) =
  # draw tank borders
  for row in 0..tank.h:
    for col in 0..tank.w:
      # draw corners
      # top left
      if row == 0 and col == 0:
        tb.write(col, row, fgWhite, "+")

      # top right
      elif row == 0 and col == tank.w:
        tb.write(col, row, fgWhite, "+")

      # bottom left
      elif row == tank.h and col == 0:
        tb.write(col, row, fgWhite, "+")

      # bottom right
      elif row == tank.h and col == tank.w:
        tb.write(col, row, fgWhite, "+")

      # draw top/bottom
      elif row == 0 or row == tank.h:
        tb.write(col, row, fgWhite, "-")

      # draw sides
      elif col == 0 or col == tank.w:
        tb.write(col, row, fgWhite, "|")

  # draw food
  for food in tank.food:
    var pos_x = nearest_int(food.pos.x)
    var pos_y = nearest_int(food.pos.y)
    # pixels[pos_y][pos_x] = food.to_string
    tb.write(pos_x, pos_y, fgWhite, food.to_string)

  # draw poop
  for poop in tank.poop:
    var pos_x = nearest_int(poop.pos.x)
    var pos_y = nearest_int(poop.pos.y)
    tb.write(pos_x, pos_y, fgWhite, ".")

  # draw fish
  for entity in tank.entities:
    var pos_x = nearest_int(entity.pos.x)
    var pos_y = nearest_int(entity.pos.y)

    tb.write(pos_x, pos_y, entity.species.color, &"{entity.name[0]}")

  tank.draw_entity_info(tb)
  discard

proc main(): void =
  # set up our terminal
  illwillInit(fullscreen=true)
  setControlCHook(exitProc)
  hideCursor()

  var tb = newTerminalBuffer(terminalWidth(), terminalHeight())
  tb.setForegroundColor(fgBlack, true)

  var tank = new_tank(60, 20)

  # add some fish species
  # var clown_fish     = new_species(name="clown_fish",     color=fgRed,    speed_max=1)
  # var angel_fish     = new_species(name="tetra_fish",     color=fgCyan,   speed_max=3)
  # var butterfly_fish = new_species(name="butterfly_fish", color=fgYellow, speed_max=2)
  var hermit_crab    = HermitCrab(color: fgWhite, speed_max: 0.5)
# 
  # var marvin = new_entity(name="Marvin",    species=angel_fish)
  # var ditzy  = new_entity(name="Ditzy",     species=angel_fish)
  # var smokey = new_entity(name="Smokey",    species=angel_fish)
  # var donald = new_entity(name="Donald",    species=clown_fish)
  # var james  = new_entity(name="James",     species=butterfly_fish)
  # # var franca = new_entity(name="Franca",    species=butterfly_fish)
  var pat    = new_entity(name="Pat Jones", species=hermit_crab)

  # tank.add_entity(marvin)
  # tank.add_entity(ditzy)
  # tank.add_entity(smokey)
  # tank.add_entity(donald)
  # tank.add_entity(james)
  # tank.add_entity(franca)
  # TODO
  # pat shouldn't swim up
  # pat should be eating poop but he's slacking off
  tank.add_entity(pat)

  while true:
    # clear the screen
    tb.clear()

    tick += 1

    if tick mod (tps * seconds_food) == 0:  # drop food every 12 seconds
      if tank.food.len < tank.food_max:
        tank.add_food()

    tank.update()

    #echo tank.to_string()
    #tb.write(0, 0, fgWhite, tank.to_string())

    # echo &"tick {tick}"
    tb.write(0, 0, fgWhite, &"tick {tick}")
    tank.draw(tb)

    #echo &"food: {tank.food.len}"

    # clean up eaten food
    tank.food = filter(tank.food, proc(f: Food): bool = f.eaten == false)

    tb.display()
    sleep(wait)

  discard

when isMainModule:
  main()