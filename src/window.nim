import logging
import math
import strformat

import sdl2

import ./liquid
import ./physics


var LOG = newFileLogger(mode=fmWrite, levelThreshold=lvlInfo, bufSize=0)
addHandler LOG

const FPS = 15
const CELL_SIZE = 40
var cells: array[cont_size, Rect]

type SDLException = object of Exception

template sdlFailIf(cond: typed, reason: string) =
  if cond: raise SDLException.newException(
    reason & ", SDL error: " & $getError())

proc draw_vector_line(renderer: RendererPtr, index, center_x, center_y: int) : void =
  let vector = water[index]
  let end_x = center_x + (int) vector.x
  let end_y = center_y + (int) vector.y
  renderer.setDrawColor(r = 0, g = 255, b = 0)
  renderer.drawLine((cint) center_x, (cint) center_y, (cint) end_x, (cint) end_y)
  discard

proc draw_all(renderer: RendererPtr): void =
  renderer.setDrawColor(r = 110, g = 132, b = 174)
  renderer.clear()

  for row in 0..<cont_height:
    for col in 0..<cont_width:
      renderer.setDrawColor(r = 0, g = 0, b = 0)
      let index = row + col
      let x = col * CELL_SIZE
      let y = row * CELL_SIZE
      let w = CELL_SIZE
      let h = CELL_SIZE
      info &"Creating rect: {x} {y} {w} {h}"
      let cell = rect((cint) x, (cint) y, (cint) w, (cint) h)
      cells[index] = cell
      renderer.drawRect(cells[index])
      # draw vector lines
      let center_x = x + (int) CELL_SIZE / 2
      let center_y = y + (int) CELL_SIZE / 2
      renderer.draw_vector_line(index, center_x, center_y)
  discard

proc main =
  echo "starting"
  sdlFailIf(not sdl2.init(INIT_VIDEO or INIT_TIMER or INIT_EVENTS)):
    "SDL2 initialization failed"

  # defer blocks get called at the end of the procedure, even if an
  # exception has been thrown
  defer: sdl2.quit()

  sdlFailIf(not setHint("SDL_RENDER_SCALE_QUALITY", "2")):
    "Linear texture filtering could not be enabled"

  let window = createWindow(title = "",
    x = SDL_WINDOWPOS_CENTERED, y = SDL_WINDOWPOS_CENTERED,
    w = CELL_SIZE * cont_width,
    h = CELL_SIZE * cont_height,
    flags = SDL_WINDOW_SHOWN)

  sdlFailIf window.isNil: "Window could not be created"
  defer: window.destroy()

  let renderer = window.createRenderer(index = -1,
    flags = Renderer_Accelerated or Renderer_PresentVsync)
  sdlFailIf renderer.isNil: "Renderer could not be created"
  defer: renderer.destroy()

  # Game loop, draws each frame
  while true:
    # Draw over all drawings of the last frame with the default
    # color
    renderer.clear()
    # Show the result on screen
    renderer.draw_all()
    delay((uint32) 1000 / FPS)
    renderer.present()

main()