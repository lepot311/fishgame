import logging
import random
import strformat

import illwill
import simple_vector

import logging
import physics


type
  Positionable* = ref object of RootObj
    pos*      : Point
    eaten*    : bool
    vec_total*: Vector2D  # accumulator of all vectors acting upon a Entity

type
  Poop* = ref object of Positionable

proc new_poop*: Poop =
  new result
  result.eaten = false
  result.vec_total = Vector(0, 0)

type
  Food* = ref object of Positionable

proc new_food*: Food =
  new result
  result.eaten = false
  result.vec_total = Vector(0, 0)

type
  Species* = ref object of RootObj
    name*     : string
    color*    : ForegroundColor
    speed_max*: float
    foods*    : seq[Positionable]

# proc new_clown_fish*(name: string): Entity
#   result = new_entity(name=name, species=ClownFish)
# new_species(name="clown_fish",     color=fgRed,    speed_max=1)

type
  Fish*          = ref object of Species
  ClownFish*     = ref object of Fish
  TetraFish*     = ref object of Fish
  ButterflyFish* = ref object of Fish

  Crawler*    = ref object of Species
  HermitCrab* = ref object of Crawler

proc new_species*(name: string, color: ForegroundColor, speed_max: float): Species =
  new result
  result.name      = name
  result.color     = color
  result.speed_max = speed_max

type
  EntityState* = enum
    IDLE     = 0
    SLEEPING = 1
    MOVING   = 2

type
  Entity* = ref object of Positionable
    # anything that can be placed in a Tank

    vec_local*: Vector2D  # the vector this Entity exerts by itself

    speed_acc*     : float
    speed_max*     : float

    # stats
    name*          : string
    food*          : int
    food_eaten*    : int
    appetite*      : int
    hunger*        : float
    species*       : Species

    # state
    state*         : EntityState
    sleep_secs*    : float
    goal*          : Positionable
    goal_threshold*: float
    has_goal*      : bool
    health*        : float
    # TODO change this to seconds
    state_tick*    : int
    # TODO change this to seconds
    food_tick*     : int

    # physics
    mass*          : float
    vel*           : float
    collides*      : bool

proc `[]=`(entity: Entity): string =
  return "test"

type EntitySeq =
  seq[Entity]

proc new_entity*(species: Species): Entity =
  new result
  result.species = species

  result.has_goal = false
  result.goal_threshold = 0.5
  result.state = EntityState.IDLE
  result.vec_local = Vector(0, 0)
  result.vec_total = Vector(0, 0)
  result.health = 100
  result.pos = Point(x: 0, y: 0)
  result.speed_acc = 1
  result.mass = 1
  result.appetite = rand(1..3)
  #result.collides_with = EntitySeq

proc is_hungry*(entity: Entity): bool =
    return entity.appetite > entity.food

proc set_goal*(entity: Entity, goal: Positionable) =
  entity.goal = goal
  entity.has_goal = true
  entity.state = EntityState.MOVING
  discard

proc sleep*(entity: Entity): void =
  entity.state = EntityState.SLEEPING
  entity.has_goal = false
  entity.state_tick = 0
  entity.sleep_secs = rand(1.0..6.0)
  discard

proc new_entity*(name: string, species: Species): Entity =
   result = new_entity(species)
   result.name = name

proc collides_with*(a: Entity, b: Entity): bool =
  return (
    abs(a.pos.x - b.pos.x) < 1 and
    abs(a.pos.y - b.pos.y) < 1
  )

proc accel*(entity: Entity): float =
  return abs(entity.vec_local.x + entity.vec_local.y)

proc school_with*(entity: Entity, other: Entity): void =
  var diff_x = other.pos.x - entity.pos.x
  var diff_y = other.pos.y - entity.pos.y

  var vec_species = Vector2D(x: diff_x, y: diff_y)

  discard vec_species.limit(1.0)

  entity.vec_local.x += vec_species.x
  entity.vec_local.y += vec_species.y

proc move_to*(entity: Entity, goal: Positionable): void =
  entity.state = EntityState.MOVING
  # Increases a entity's vector in the direction of the x, y goal
  var diff_x = goal.pos.x - entity.pos.x
  var diff_y = goal.pos.y - entity.pos.y

  var vec_goal = Vector(diff_x, diff_y)
  entity.vec_local = vec_goal
  discard entity.vec_local.limit(entity.species.speed_max)

  #if entity.goal.pos.x > entity.pos.x:
    #if vector_goal.x > entity.species.speed_max:
      #entity.vec_local.x += min(entity.speed_acc, entity.species.speed_max - entity.vec_local.x)
    #else:
      #entity.vec_local.x -= entity.speed_acc
  #else:
    #if vector_goal.x < entity.species.speed_max:
      #entity.vec_local.x -= min(entity.speed_acc, entity.species.speed_max - entity.vec_local.x)
    #else:
      #entity.vec_local.x += entity.speed_acc

  #if vector_goal.y > entity.vec_local.y:
    #entity.vec_local.y += min(entity.speed_acc, entity.species.speed_max - entity.vec_local.y)
  #else:
    #if vector_goal.y < entity.species.speed_max:
      #entity.vec_local.y -= min(entity.speed_acc, entity.species.speed_max - entity.vec_local.y)
    #else:
      #entity.vec_local.y += entity.speed_acc

  discard

proc at_goal*(entity: Entity): bool =
  info(
    entity.name, " [goal distance] ",
    "x: ", abs(entity.pos.x - entity.goal.pos.x),
    " y: ", abs(entity.pos.y - entity.goal.pos.y)
  )
  return (
    (abs(entity.pos.x - entity.goal.pos.x) <= entity.goal_threshold) and
    (abs(entity.pos.y - entity.goal.pos.y) <= entity.goal_threshold)
  )