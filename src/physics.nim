import simple_vector


let GRAVITY* = Vector2D(x: 0, y: 0.2)

type
  Point* = object
    x*: float
    y*: float