# Package

version       = "0.1.0"
author        = "Erik Potter"
description   = "An aquarium simulator"
license       = "MIT"
srcDir        = "src"
binDir        = "bin"
bin           = @["game"]



# Dependencies

requires "nim >= 1.2.6"
requires "illwill"
requires "sdl2"
requires "simple_vector"