import unittest

import simple_vector

import physics
import entity


suite "Entity":
    test "Entity constructor":
        let entity = new_entity()

        check entity.pos.x == 0
        check entity.pos.y == 0
        check entity.vec_local.x == 0
        check entity.vec_local.y == 0
        check entity.health == 100

    suite "Entity: local acceleration":
        setup:
            let entity = new_entity()

        test "An entity at rest has no acceleration":
            entity.vec_local = Vector(0, 0)

            check entity.accel == 0

        test "An entity with local motion has acceleration":
            entity.vec_local = Vector(1, 0)

            check entity.accel == 1

    suite "Entity: collision detection":
        setup:
            let entityA = new_entity()
            let entityB = new_entity()

        test "Fish at the same postion collide":
            check entityA.collides_with entityB

        test "Fish at different positions don't collide":
            entityB.pos.x = 1
            entityB.pos.y = 1

            check not entityA.collides_with entityB

    suite "Entity: swim to":
        setup:
            let entity = new_entity()

        suite "Entity: swim to: straight line":
            test "Given a goal Point, an entity adjusts vector towards it":
                check entity.pos.x == 0
                check entity.pos.y == 0
                check entity.vec_local.x == 0
                check entity.vec_local.y == 0

                let goal = Positionable()
                goal.pos = Point(x: 1, y: 0)

                entity.move_to goal

                check entity.vec_local.mag() == 1

            test "Entity local vector magniture is limited by max speed":
                let goal = Positionable()
                goal.pos = Point(x: 2, y: 0)

                entity.move_to goal

                check entity.vec_local.mag() == 1

            test "Entity at rest adjusts to prevent from overshooting goal":
                let goal = Positionable()
                goal.pos = Point(x: 0.5, y: 0)

                entity.move_to goal

                check entity.vec_local.mag() == 0.5 

            test "Entity in motion slows to prevent from overshooting goal":
                # start with a local vector
                entity.vec_local = Vector(2, 0)

                let goal = Positionable()
                goal.pos = Point(x: 0.5, y: 0)

                entity.move_to goal

                check entity.vec_local.mag() == 0.5 

            test "Entity goal threshold":
                let goal = Positionable()
                goal.pos = Point(x: 1, y: 0)

                entity.goal = goal

                entity.goal_threshold = 0
                check not entity.at_goal

                entity.goal_threshold = 1
                check entity.at_goal