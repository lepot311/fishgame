import unittest

import entity


suite "Entity with state controller":
    setup:
        let entity = new_entity()

    test "Entity constructor":
        check entity.pos.x == 0
        check entity.pos.y == 0
        check entity.vec_local.x == 0
        check entity.vec_local.y == 0