import unittest

import simple_vector

import entity
import physics
import tank


suite "Tank":
    test "Tank constructor":
        let tank = new_tank(w=10, h=10)

    test "Tank has expected dimensions":
        let tank = new_tank(10, 10)

        check tank.w == 10
        check tank.h == 10

    test "Entity movement":
        let entity = new_entity()
        let tank = new_tank(w=10, h=10)

        # increase the local vector of the fish in the x axis
        check entity.pos.x == 0
        check entity.pos.y == 0

        entity.vec_local = Vector(1, 2)

        tank.update_pos(entity)

        check entity.pos.x == 1.0
        check entity.pos.y == 2.0

    suite "Tank: Entity with goal":
        setup:
            let tank = new_tank(3, 3)
            let entity = new_entity()

            entity.vec_local = Vector(0, 0)
            entity.speed_max = 1
            entity.goal_threshold = 0

        test "Entity reaches goal":
            let goal = Positionable()
            goal.pos = Point(x: 2.5, y: 0)

            entity.move_to goal
            tank.update_pos entity  # step 1

            check entity.pos.x == 1
            check entity.pos.y == 0

            entity.move_to goal
            tank.update_pos entity  # step 2

            check entity.pos.x == 2
            check entity.pos.y == 0

            entity.move_to goal
            tank.update_pos entity  # step 3

            check entity.pos.x == 2.5
            check entity.pos.y == 0

        suite "Tank: Entity with goal: Entity stays within tank bounds":
            test "X > Tank width":
                let goal = Positionable()
                goal.pos = Point(x: 50, y: 0)
                
                # step a few times
                for i in 1..20:
                    entity.move_to goal
                    tank.update_pos entity

                check entity.pos.x == 2
                check entity.pos.y == 0

            test "X < 0":
                let goal = Positionable()
                goal.pos = Point(x: -50, y: 0)
                
                # step a few times
                for i in 1..20:
                    entity.move_to goal
                    tank.update_pos entity

                check entity.pos.x == 0
                check entity.pos.y == 0

            test "Y > Tank height":
                let goal = Positionable()
                goal.pos = Point(x: 0, y: 50)
                
                # step a few times
                for i in 1..20:
                    entity.move_to goal
                    tank.update_pos entity

                check entity.pos.x == 0
                check entity.pos.y == 2

            test "Y < 0":
                let goal = Positionable()
                goal.pos = Point(x: 0, y: -50)
                
                # step a few times
                for i in 1..20:
                    entity.move_to goal
                    tank.update_pos entity

                check entity.pos.x == 0
                check entity.pos.y == 0

    test "Tank constructor":
        let tank = new_tank(w=10, h=10)

    test "Tank has expected dimensions":
        let tank = new_tank(10, 10)

        check tank.w == 10
        check tank.h == 10

    test "Food with gravity doesn't fall out of tank":
        let tank = new_tank(3, 3)
        let food = new_food()

        tank.food.add food

        for i in 1..20:
            tank.update food

        check food.pos.x == 0
        check food.pos.y == 2.0

    suite "Tank: Entity with goal":
        setup:
            let tank = new_tank(3, 3)

            let entity = new_entity()
            entity.vec_local = Vector(0, 0)
            entity.speed_max = 1
            entity.goal_threshold = 0

        test "Entity reaches goal":
            let goal = Positionable()
            goal.pos = Point(x: 1.5, y: 0)

            entity.move_to goal
            tank.update_pos entity  # step 1

            check entity.pos.x == 1
            check entity.pos.y == 0

            entity.move_to goal
            tank.update_pos entity  # step 2

            check entity.pos.x == 1.5
            check entity.pos.y == 0

        suite "Tank: Entity with goal: Entity stays within tank bounds":
            test "X > Tank width":
                let goal = Positionable()
                goal.pos = Point(x: 50, y: 0)
                
                # step a few times
                for i in 1..20:
                    entity.move_to goal
                    tank.update_pos entity

                check entity.pos.x == 2
                check entity.pos.y == 0

            test "X < 0":
                let goal = Positionable()
                goal.pos = Point(x: -50, y: 0)
                
                # step a few times
                for i in 1..20:
                    entity.move_to goal
                    tank.update_pos entity

                check entity.pos.x == 0
                check entity.pos.y == 0

            test "Y > Tank height":
                let goal = Positionable()
                goal.pos = Point(x: 0, y: 50)
                
                # step a few times
                for i in 1..20:
                    entity.move_to goal
                    tank.update_pos entity

                check entity.pos.x == 0
                check entity.pos.y == 2

            test "Y < 0":
                let goal = Positionable()
                goal.pos = Point(x: 0, y: -50)
                
                # step a few times
                for i in 1..20:
                    entity.move_to goal
                    tank.update_pos entity

                check entity.pos.x == 0
                check entity.pos.y == 0

        suite "Tank: Entity with goal: And food":
            setup:
                let entity = new_entity()
                let tank = new_tank(3, 3)
                let food = new_food()
                food.pos = Point(x: 2, y: 2)

            test "Entity switches current goal to food":
                let goal = Positionable()
                goal.pos = Point(x: 2, y: 0)

                tank.add_entity entity
                entity.set_goal goal
                tank.update()

                # food is added...
                tank.food.add food
                check entity.goal == goal
                tank.update()

                check entity.goal == food