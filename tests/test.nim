import illwill
import unittest

import physics
import entity


suite "Point":
    test "Point has expected coords after init":
        let pos = Point(x: 0, y: 0)
        check pos.x == 0
        check pos.y == 0


suite "Food":
    test "Init some food":
        let food = Food(eaten: false)
        food.eaten = true


suite "Species":
    test "Init a species":
        let species = Species(name: "dachshund", color: fgBlack)